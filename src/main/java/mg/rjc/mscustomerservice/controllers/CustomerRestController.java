package mg.rjc.mscustomerservice.controllers;

import lombok.RequiredArgsConstructor;
import mg.rjc.mscustomerservice.dto.CustomerRequestDTO;
import mg.rjc.mscustomerservice.dto.CustomerResponseDTO;
import mg.rjc.mscustomerservice.services.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/api")
public class CustomerRestController {
    private final CustomerService customerService;

    @GetMapping(path = "/customers")
    public List<CustomerResponseDTO> getAllCustomers() {
        return customerService.getCustomers();
    }

    @PostMapping(path = "/customers")
    public CustomerResponseDTO saveCustomer(@RequestBody CustomerRequestDTO customerRequestDTO) {
        customerRequestDTO.setId(UUID.randomUUID().toString());
        return customerService.save(customerRequestDTO);
    }

    @GetMapping(path = "/customers/{id}")
    public CustomerResponseDTO getCustomerById(@PathVariable("id") String id) {
        return customerService.getCustomerById(id);
    }
}
