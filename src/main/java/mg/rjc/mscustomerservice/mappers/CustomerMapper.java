package mg.rjc.mscustomerservice.mappers;

import mg.rjc.mscustomerservice.dto.CustomerRequestDTO;
import mg.rjc.mscustomerservice.dto.CustomerResponseDTO;
import mg.rjc.mscustomerservice.entities.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper {
    CustomerResponseDTO customerToCustomerResponseDTO(Customer customer);
    Customer customerRequestDTOToCustomer(CustomerRequestDTO customerRequestDTO);
}
