package mg.rjc.mscustomerservice;

import mg.rjc.mscustomerservice.dto.CustomerRequestDTO;
import mg.rjc.mscustomerservice.services.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.UUID;

@SpringBootApplication
public class MsCustomerServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsCustomerServiceApplication.class, args);
    }

    @Bean
    CommandLineRunner start(CustomerService customerService) {
        return args -> {
            customerService.save(new CustomerRequestDTO(UUID.randomUUID().toString(), "Johns", "johns@aria.com"));
            customerService.save(new CustomerRequestDTO(UUID.randomUUID().toString(), "James", "james@aria.com"));
        };
    }

}
