package mg.rjc.mscustomerservice.services;

import lombok.RequiredArgsConstructor;
import mg.rjc.mscustomerservice.dto.CustomerRequestDTO;
import mg.rjc.mscustomerservice.dto.CustomerResponseDTO;
import mg.rjc.mscustomerservice.entities.Customer;
import mg.rjc.mscustomerservice.mappers.CustomerMapper;
import mg.rjc.mscustomerservice.repositories.CustomerRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;

    @Override
    public CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO) {
        Customer customer = customerMapper.customerRequestDTOToCustomer(customerRequestDTO);
        return customerMapper.customerToCustomerResponseDTO(customerRepository.save(customer));
    }

    @Override
    public CustomerResponseDTO getCustomerById(String id) {
        Customer customer = customerRepository.findById(id).get();
        return customerMapper.customerToCustomerResponseDTO(customer);
    }

    @Override
    public CustomerResponseDTO update(CustomerRequestDTO customerRequestDTO) {
        Customer customer = customerMapper.customerRequestDTOToCustomer(customerRequestDTO);
        return customerMapper.customerToCustomerResponseDTO(customerRepository.save(customer));
    }

    @Override
    public List<CustomerResponseDTO> getCustomers() {
        List<Customer> customers = customerRepository.findAll();
        return customers.stream().map(customerMapper::customerToCustomerResponseDTO).collect(Collectors.toList());
    }
}
