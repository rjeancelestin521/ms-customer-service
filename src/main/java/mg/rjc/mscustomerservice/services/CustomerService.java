package mg.rjc.mscustomerservice.services;

import mg.rjc.mscustomerservice.dto.CustomerRequestDTO;
import mg.rjc.mscustomerservice.dto.CustomerResponseDTO;

import java.util.List;

public interface CustomerService {
    CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO);
    CustomerResponseDTO getCustomerById(String id);
    CustomerResponseDTO update(CustomerRequestDTO customerRequestDTO);
    List<CustomerResponseDTO> getCustomers();
}
