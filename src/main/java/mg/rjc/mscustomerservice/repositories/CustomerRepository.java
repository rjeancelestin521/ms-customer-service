package mg.rjc.mscustomerservice.repositories;

import mg.rjc.mscustomerservice.entities.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, String> {
}
