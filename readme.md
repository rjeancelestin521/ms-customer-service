# ms-customer-service

ms-customer-service est un API microservice

## Environnement de développement

### Pré-requis

* Java 11
* maven
* lombok
* Spring Data JPA
* Spring Web
* H2 Database
* Eureka Client
* Map Struct
* Spring doc openapi-ui

## Nom microservice
```bash
CUSTOMER-SERVICE
```

## Fichier de configuration application.properties
```bash
server.port=8082
spring.application.name=CUSTOMER-SERVICE
spring.h2.console.enabled=true
spring.cloud.discovery.enabled=false
spring.datasource.url=jdbc:h2:mem:customer-db
```
## Accès aux documentations (api)
```bash
http://localhost:8082/v3/api-docs
http://localhost:8082/swagger-ui.html
```
